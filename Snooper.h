//
//  Snooper.h
//  SnoopX
//
//  Created by tzhuan on 2008/8/4.
//  Copyright 2008 NTU CSIE CMLAB. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import <ApplicationServices/ApplicationServices.h>

#define SPACING 6

typedef enum {
	BLUE_INDEX, GREEN_INDEX, RED_INDEX, ALPHA_INDEX
} ColorIndex;

// Synchronize with tags of Graph menu items
typedef enum {
	GSNone, GSHoriStep, GSHoriLinear, GSVertStep, GSVertLinear, GSStatistics
} GraphState;

@interface Snooper : NSObject {
	size_t zoom;
	CGFloat maxBackingScaleFactor;

	// center pixel channels
	size_t cursorRed, cursorGreen, cursorBlue, cursorAlpha;

	size_t mainScreenHeight;

	size_t capturePointWidth, capturePointHeight;
	size_t zoomPointWidth, zoomPointHeight;
	size_t viewPointWidth, viewPointHeight;
	NSRect capturePointRect;

	// origin is upper-left
	size_t capturePixelCenterX, capturePixelCenterY;

	// origin is lower-left
	size_t zoomPixelCenterX, zoomPixelCenterY;

	size_t gridPixelSize;

	size_t capturePixelWidth, capturePixelHeight;
	size_t zoomPixelWidth, zoomPixelHeight;
	size_t viewPixelWidth, viewPixelHeight;
	size_t cropPixelX, cropPixelY;

	CGContextRef captureContext, zoomContext, viewContext;

	int cursorPointX, cursorPointY;
	int cursorPixelX, cursorPixelY;

	// in pixel
	unsigned char* captureData;
	size_t captureDataWidth;
	size_t captureDataHeight;
	size_t captureDataBytesPerRow;
	// size_t captureDataCenterX, captureDataCenterY;

	// CGColor objects
	CGColorRef cgRed, cgGreen, cgBlue, cgWhite, cgBlack, cgGray;

	// For CGBitmapContext
	CGColorSpaceRef colorSpace;
	CGImageAlphaInfo alphaInfo;
	CGBitmapInfo bitmapInfo;
	size_t bitsPerComponent;
	size_t bitsPerPixel;
	size_t bytesPerPixel;

	GraphState graph;
	BOOL highlight;
	BOOL statisticsInfo;

	char statisticsInfoString[256];
}

@property(readonly) size_t cursorRed;;
@property(readonly) size_t cursorGreen;;
@property(readonly) size_t cursorBlue;;
@property(readonly) size_t cursorAlpha;;
@property(readwrite, assign) BOOL highlight;
@property(readwrite, assign) BOOL statisticsInfo;
@property(readwrite, assign) GraphState graph;

- (NSImage*) snoopFrom:(NSArray*)screens;
- (void) setZoom:(size_t)z;
- (void) setCursorPointX:(int)x Y:(int)y;
- (void) setViewPointWidth:(size_t)width Height:(size_t)height;
- (void) resetPointSizes;
- (void) resetPixelSizes;
- (void) resetCapturePointRect;
- (CGImageRef) getScreenshotFor:(id)screen In:(CGRect)rect;
- (CGContextRef) initCaptureContextWithImage:(CGImageRef)screenshot;
- (unsigned char*) initCaptureData;
- (CGContextRef) initZoomContext;
- (CGContextRef) getCaptureContextFrom:(NSArray*)screens;
- (CGContextRef) getZoomContext;
- (CGImageRef) getCropImage;
- (void) captureCursorColor;
- (void) drawImage:(CGImageRef)image At:(CGRect)rect;
- (void) setMainScreenHeight:(size_t)height;
- (void) setMaxBackingScaleFactor:(CGFloat)factor;

- (void) drawCenter;
- (void) drawHorizontalGraph;
- (void) drawVerticalGraph;
- (void) drawGraphCanvasWithRect:(CGRect)rect;
- (void) drawGraph;
- (void) drawStatistics;

- (NSString*) createStatisticsInfoString;

// - (unsigned char*) getNormalizedBitmapDataFrom:(CGImageRef)image with:(BOOL*)shouldFree;

@end
