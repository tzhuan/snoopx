//
//  main.m
//  SnoopX
//
//  Created by tzhuan on 2008/7/21.
//  Copyright NTU CSIE CMLAB 2008. All rights reserved.
//

#import <Cocoa/Cocoa.h>

int main(int argc, char *argv[])
{
    return NSApplicationMain(argc,  (const char **) argv);
}
