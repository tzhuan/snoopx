//
//  Controller.m
//  SnoopX
//
//  Created by tzhuan on 2008/7/21.
//  Copyright 2008 NTU CSIE CMLAB. All rights reserved.
//

#import "Controller.h"
#import "Snooper.h"

#define KEYCODE_LEFT_ARROW 123
#define KEYCODE_RIGHT_ARROW 124
#define KEYCODE_DOWN_ARROW 125
#define KEYCODE_UP_ARROW 126

@implementation Controller

+(void) initialize 
{
#ifndef NDEBUG
	NSLog(@"[Controller initialize]");
#endif
	
	// default preference
	NSArray *keys = [
		NSArray arrayWithObjects:
		@"Zoom", @"Time", @"AlwaysOnTop", @"Graph", @"Highlight", @"LockPosition", @"StatisticsInfo", nil
	];
	NSArray *objects = [
		NSArray arrayWithObjects:
		@"1", @"50", @"YES", @"0", @"YES", @"NO", @"YES", nil
	];
	NSDictionary *appDefaults = [NSDictionary dictionaryWithObjects:objects
															forKeys:keys];
	// register the default preference
	[[NSUserDefaults standardUserDefaults] registerDefaults:appDefaults];
	
}

-(id)init
{
#ifndef NDEBUG
	NSLog(@"[Controller init]");
#endif
	if ((self = [super init])) {
		snooper = [[Snooper alloc] init];
		timer = nil;
		currentGraph = nil;
		currentZoom = nil;
		currentTimer = nil;

		keydownHandler = ^NSEvent * (NSEvent * event) {
			CGEventRef getEvent = CGEventCreate(NULL);
			CGPoint mouse = CGEventGetLocation(getEvent);
			CFRelease(getEvent);
			int dx = 0;
			int dy = 0;
			switch ([event keyCode]) {
			case KEYCODE_LEFT_ARROW:
				dx = -1;
				break;
			case KEYCODE_RIGHT_ARROW:
				dx = 1;
				break;
			case KEYCODE_DOWN_ARROW:
				dy = 1;
				break;
			case KEYCODE_UP_ARROW:
				dy = -1;
				break;
			default:
				return event;
			}
			if ([event modifierFlags] & NSShiftKeyMask) {
				dx *= 10;
				dy *= 10;
			}
			/* // control + up/down is global shortcut!
			if ([event modifierFlags] & NSControlKeyMask) {
				dx *= 1e10;
				dy *= 1e10;
			}
			*/
			mouse.x += dx;
			mouse.y += dy;
			CGWarpMouseCursorPosition(mouse);
			return nil;
		};
		keydownMonitorId = [NSEvent addLocalMonitorForEventsMatchingMask:NSKeyDownMask handler:keydownHandler];
	}
	return self;
}

- (void)awakeFromNib
{
	[super awakeFromNib];
	[self loadUserDefaults];
}

- (BOOL) applicationShouldTerminateAfterLastWindowClosed:(NSApplication*)app
{
	return YES;
}

- (BOOL) applicationShouldHandleReopen:(NSApplication *)app hasVisibleWindows:(BOOL)flag
{
	if (flag) {
		return NO;
	} else {
		[window makeKeyAndOrderFront:self];
		return YES;	
	}
}

-(void) loadUserDefaults
{
#ifndef NDEBUG
	NSLog(@"[Controller loadUserDefaults]");
#endif
	NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
	[self updateZoom:[defaults integerForKey:@"Zoom"] Sync:NO];
	[self updateAlwaysOnTop:[defaults boolForKey:@"AlwaysOnTop"] Sync:NO];
	[self updateHighlight:[defaults boolForKey:@"Highlight"] Sync:NO];
	[self updateLockPosition:[defaults boolForKey:@"LockPosition"] Sync:NO];
	[self updateGraph:[defaults integerForKey:@"Graph"] Sync:NO];
	[self updateStatisticsInfo:[defaults integerForKey:@"StatisticsInfo"] Sync:NO];
	[self updateTimer:[defaults integerForKey:@"Time"] Sync:NO];
}

-(void) dealloc 
{
#ifndef NDEBUG
	NSLog(@"[Controller dealloc]\n");
#endif
	[snooper release];
	[super dealloc];
}

-(void)updateZoom:(int)zoom Sync:(BOOL)sync
{
#ifndef NDEBUG
	NSLog(@"[Controller updateZoom:%d]", zoom);
#endif
	NSMenuItem *items[] = {
		zoom1, zoom2, zoom3, zoom4, zoom5,
		zoom6, zoom7, zoom8, zoom9, zoom10
	};

	if (currentZoom)
		[currentZoom setState:NSOffState];
	[items[zoom-1] setState:NSOnState];
	currentZoom = items[zoom-1];
	[snooper setZoom:zoom];

	if (sync) {
		[[NSUserDefaults standardUserDefaults] setInteger:zoom forKey:@"Zoom"];
		[[NSUserDefaults standardUserDefaults] synchronize];
	}
}

-(void)updateHighlight:(BOOL)flag Sync:(BOOL)sync
{
#ifndef NDEBUG
	NSLog(@"[Controller updateHighlight:%d]", flag);
#endif
	[highlight setState:(flag ? NSOnState: NSOffState)];
	[snooper setHighlight:flag];
	
	if (sync) {
		[[NSUserDefaults standardUserDefaults] setBool:flag forKey:@"Highlight"];
		[[NSUserDefaults standardUserDefaults] synchronize];
	}
}

-(void)updateStatisticsInfo:(BOOL)flag Sync:(BOOL)sync
{
#ifndef NDEBUG
	NSLog(@"[Controller updateStatisticsInfo:%d]", flag);
#endif
	[statisticsInfo setState:(flag ? NSOnState: NSOffState)];
	[snooper setStatisticsInfo:flag];
	
	if (sync) {
		[[NSUserDefaults standardUserDefaults] setBool:flag forKey:@"StatisticsInfo"];
		[[NSUserDefaults standardUserDefaults] synchronize];
	}
}

-(void)updateLockPosition:(BOOL)flag Sync:(BOOL)sync
{
#ifndef NDEBUG
	NSLog(@"[Controller updateLockPosition:%d]", flag);
#endif
	[lockPosition setState:(flag ? NSOnState: NSOffState)];
	
	if (sync) {
		[[NSUserDefaults standardUserDefaults] setBool:flag forKey:@"LockPosition"];
		[[NSUserDefaults standardUserDefaults] synchronize];
	}
}

-(void)updateAlwaysOnTop:(BOOL)flag Sync:(BOOL)sync
{
#ifndef NDEBUG
	NSLog(@"[Controller updateAlwaysOnTop:%d]", flag);
#endif
	if (flag) {
		[alwaysOnTop setState:NSOnState];
		[[NSApp mainWindow] setLevel:NSScreenSaverWindowLevel];
	} else {
		[alwaysOnTop setState:NSOffState];
		[[NSApp mainWindow] setLevel:NSNormalWindowLevel];
	}

	if (sync) {
		[[NSUserDefaults standardUserDefaults] setBool:flag forKey:@"AlwaysOnTop"];
		[[NSUserDefaults standardUserDefaults] synchronize];
	}
}

-(void)updateTimer:(int)time Sync:(BOOL)sync
{
#ifndef NDEBUG
	NSLog(@"[Controller updateTimer:%d]", time);
#endif
	NSMenuItem *items[] = {
		timerNone, timer50, timer100, timer200, timer500, timer1000
	};
	int times[] = {0, 50, 100, 200, 500, 1000};
	int size = sizeof(times)/sizeof(int);
	int index = -1;
	for (int i = 0; i < size; ++i)
		if (time == times[i]) {
			index = i;
			break;
		}
	if (index >= size) index = 0;

	if (currentTimer)
		[currentTimer setState:NSOffState];
	[items[index] setState:NSOnState];
	currentTimer = items[index];
	[self stopTimer];
	if (time)
		[self startTimer:time/1000.0f];

	if (sync) {
		[[NSUserDefaults standardUserDefaults] setInteger:time forKey:@"Time"];
		[[NSUserDefaults standardUserDefaults] synchronize];
	}
}
	
-(void)updateGraph:(int)graph Sync:(BOOL)sync
{
#ifndef NDEBUG
	NSLog(@"[Controller updateGraph:%d]", graph);
#endif
	NSMenuItem *items[] = {
		graphOff, 
		graphHoriStep, graphHoriLinear, 
		graphVertStep, graphVertLinear,
		graphStatistics
	};
	if (currentGraph)
		[currentGraph setState:NSOffState];
	[items[graph] setState:NSOnState];
	currentGraph = items[graph];
	[snooper setGraph:graph];

	if (graph == GSNone) {
#ifndef NDEBUG
		NSLog(@"currentGraph: off");
#endif
		[highlight setEnabled:FALSE];
		[statisticsInfo setEnabled:FALSE];
	} else if (graph == GSStatistics) {
#ifndef NDEBUG
		NSLog(@"currentGraph: statistics");
#endif
		[highlight setEnabled:FALSE];
		[statisticsInfo setEnabled:TRUE];
	} else {
#ifndef NDEBUG
		NSLog(@"currentGraph: graph");
#endif
		[highlight setEnabled:TRUE];
		[statisticsInfo setEnabled:FALSE];
	}

	if (sync) {
		[[NSUserDefaults standardUserDefaults] setInteger:graph forKey:@"Graph"];
		[[NSUserDefaults standardUserDefaults] synchronize];
	}
}

-(void)updateView
{
	// get the mouse location
	if ([lockPosition state] == NSOffState) {
		cursor = [NSEvent mouseLocation];
		cursor.x = floorf(cursor.x);
		cursor.y = ceilf(cursor.y);
	}

#ifndef NDEBUG
	NSLog(@"cursor: %@", NSStringFromPoint(cursor));
#endif

	BOOL first = YES;
	size_t mainScreenHeight = 0;
	CGFloat maxFactor = 0.0;
	NSArray* screens = [NSScreen screens];
	for (id screen in screens) {
		if (first) {
			// first screen is the main screen
			NSRect screenRect = [screen frame];
			mainScreenHeight = screenRect.size.height;
			first = NO;
		}
		CGFloat factor = [screen backingScaleFactor];
		if (factor > maxFactor)
			maxFactor = factor;
	}

	int flippedCursorX = floor(cursor.x);
	int flippedCursorY = ceil(mainScreenHeight - cursor.y);

	NSRect rect = [view bounds];
	[snooper setMainScreenHeight:mainScreenHeight];
	[snooper setMaxBackingScaleFactor:maxFactor];
	[snooper setCursorPointX:floor(cursor.x) Y:ceil(cursor.y)];
	[snooper setViewPointWidth:rect.size.width Height:rect.size.height];
	
	NSImage *image = [snooper snoopFrom:screens];

	if ([snooper graph] == GSStatistics && [snooper statisticsInfo]) {
		NSString* string = [snooper createStatisticsInfoString];
		NSFont* font = [NSFont fontWithName:@"Menlo" size:11.0];
		NSDictionary* attributes = [NSDictionary 
			dictionaryWithObjectsAndKeys:font, NSFontAttributeName,
			[NSColor whiteColor], NSForegroundColorAttributeName,
			nil];
		NSRect rect = NSOffsetRect(
			[string boundingRectWithSize:[image size] options:NSStringDrawingUsesLineFragmentOrigin attributes:attributes],
			SPACING, SPACING
		);
		const size_t spacing = 2;
		rect.size.width += 4*spacing;
		rect.size.height += 2*spacing;

		// draw the statistics info string
		[image lockFocus];
		[[NSColor blackColor] set];
		[NSBezierPath fillRect: rect];
		[[NSColor whiteColor] set];
		[NSBezierPath strokeRect: rect];
		[string drawAtPoint:NSMakePoint(SPACING+2*spacing, SPACING+spacing) withAttributes:attributes];
		[image unlockFocus];

		[string release];
	}

	[view setImage:image];
	[image autorelease];
	
	[status setStringValue: [
		NSString stringWithFormat:@"(% 05d,% 05d) - (%3zu,%3zu,%3zu)",
			flippedCursorX, flippedCursorY,
			[snooper cursorRed], [snooper cursorGreen], [snooper cursorBlue]
		]
	];
}

-(IBAction)actUpdate:(id)sender
{
	[self updateView];
}

-(IBAction)actGraph:(id)sender
{
 	[self updateGraph:[sender tag] Sync:YES];
	[self updateView];
}

-(IBAction)actZoom:(id)sender
{
	[self updateZoom:[sender tag] Sync:YES];
	[self updateView];
}

-(IBAction)actTimer:(id)sender
{	
	[self updateTimer:[sender tag] Sync:YES];
}

-(IBAction)actAlwaysOnTop:(id)sender
{
	[self updateAlwaysOnTop:([sender state] == NSOffState) Sync:YES];
}

-(IBAction)actHighlight:(id)sender
{
	[self updateHighlight:([sender state] == NSOffState) Sync:YES];
}

-(IBAction)actStatisticsInfo:(id)sender
{
	[self updateStatisticsInfo:([sender state] == NSOffState) Sync:YES];
}

-(IBAction)actLockPosition:(id)sender
{
	[self updateLockPosition:([sender state] == NSOffState) Sync:YES];
}

-(void)startTimer:(float)time
{
#ifndef NDEBUG
	NSLog(@"[Controller startTimer:%f]", time);
#endif
	timer = [
		NSTimer scheduledTimerWithTimeInterval:time
										target:self
									  selector:@selector(updateView)
									  userInfo:NULL
									   repeats:YES];
}

-(void)stopTimer
{
	[timer invalidate];
	timer = nil;
}

@end
