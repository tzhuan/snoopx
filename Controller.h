//
//  Controller.h
//  SnoopX
//
//  Created by tzhuan on 2008/7/21.
//  Copyright 2008 NTU CSIE CMLAB. All rights reserved.
//

#import <Cocoa/Cocoa.h>

#import "Snooper.h"

@interface Controller : NSObject {
	IBOutlet NSWindow *window;
	IBOutlet NSImageView *view;
	IBOutlet NSTextField *status;

	NSMenuItem *currentGraph;
	IBOutlet NSMenuItem *graphOff;
	IBOutlet NSMenuItem *graphHoriStep;
	IBOutlet NSMenuItem *graphHoriLinear;
	IBOutlet NSMenuItem *graphVertStep;
	IBOutlet NSMenuItem *graphVertLinear;
	IBOutlet NSMenuItem *graphStatistics;

	NSMenuItem *currentZoom;
	IBOutlet NSMenuItem *zoom1;
	IBOutlet NSMenuItem *zoom2;
	IBOutlet NSMenuItem *zoom3;
	IBOutlet NSMenuItem *zoom4;
	IBOutlet NSMenuItem *zoom5;
	IBOutlet NSMenuItem *zoom6;
	IBOutlet NSMenuItem *zoom7;
	IBOutlet NSMenuItem *zoom8;
	IBOutlet NSMenuItem *zoom9;
	IBOutlet NSMenuItem *zoom10;

	NSMenuItem *currentTimer;
	IBOutlet NSMenuItem *timerNone;
	IBOutlet NSMenuItem *timer50;
	IBOutlet NSMenuItem *timer100;
	IBOutlet NSMenuItem *timer200;
	IBOutlet NSMenuItem *timer500;
	IBOutlet NSMenuItem *timer1000;

	IBOutlet NSMenuItem *lockPosition;
	
	IBOutlet NSMenuItem *alwaysOnTop;

	IBOutlet NSMenuItem *highlight;
	IBOutlet NSMenuItem *statisticsInfo;

	NSTimer *timer;
	Snooper *snooper;
	NSPoint cursor;

	NSEvent * (^keydownHandler)(NSEvent *);
	id keydownMonitorId;
}

-(IBAction)actGraph:(id)sender;
-(IBAction)actUpdate:(id)sender;
-(IBAction)actZoom:(id)sender;
-(IBAction)actTimer:(id)sender;
-(IBAction)actAlwaysOnTop:(id)sender;
-(IBAction)actHighlight:(id)sender;
-(IBAction)actStatisticsInfo:(id)sender;
-(IBAction)actLockPosition:(id)sender;

-(void)loadUserDefaults;

-(void)updateZoom:(int)zoom Sync:(BOOL)sync;
-(void)updateAlwaysOnTop:(BOOL)flag Sync:(BOOL)sync;
-(void)updateTimer:(int)freq Sync:(BOOL)sync;
-(void)updateGraph:(int)graph Sync:(BOOL)sync;
-(void)updateHighlight:(BOOL)flag Sync:(BOOL)sync;
-(void)updateStatisticsInfo:(BOOL)flag Sync:(BOOL)sync;
-(void)updateLockPosition:(BOOL)flag Sync:(BOOL)sync;

-(void)updateView;
-(void)stopTimer;
-(void)startTimer:(float)time;
@end
