//  Snooper.m
//  SnoopX
//
//  Created by tzhuan on 2008/8/4.
//  Copyright 2008 NTU CSIE CMLAB. All rights reserved.
//

#import "Snooper.h"

/**
 * Options:
 *   CGWindowListCreateImage
 *   CGDisplayCreateImageForRect with CGBitmapContext
 *   CGBitmapContext with raw bitmap data
 */
// #define CAPTURE_USING_CG_WINDOW_LIST_CREATE_IMAGE
// #define CAPTURE_USING_CG_DISPLAY_CREATE_IMAGE_FOR_RECT_BITMAP_CONTEXT
#define CAPTURE_USING_CG_DISPLAY_CREATE_IMAGE_FOR_RECT_BITMAP_DATA

void clearBitmapData(unsigned char* data, size_t width, size_t height, size_t bytesPerPixel, size_t bytesPerRow)
{
	unsigned char* row = data;
	for (size_t h = 0; h < height; ++h) {
		unsigned char* pixel = row;
		for (size_t w = 0; w < width; ++w) {
			pixel[ALPHA_INDEX] = 255;
			pixel += bytesPerPixel;
		}
		row += bytesPerRow;
	}
}

void drawBitmapData(
	const unsigned char* src, size_t srcWidth, size_t srcHeight, size_t srcBytesPerRow,
	unsigned char* dst, size_t dstWidth, size_t dstHeight, size_t dstBytesPerRow,
	size_t bytesPerPixel,
	size_t rectX, size_t rectY, size_t rectWidth, size_t rectHeight
) {
#ifndef NDEBUG
	NSLog(@"drawBitmapData(%p, %zu, %zu, %zu, "
		"%p, %zu, %zu, %zu, "
		"%zu, "
		"%zu, %zu, %zu, %zu)",
		src, srcWidth, srcHeight, srcBytesPerRow,
		dst, dstWidth, dstHeight, dstBytesPerRow,
		bytesPerPixel,
		rectX, rectY, rectWidth, rectHeight
	);
#endif
	const unsigned char* srcRow = src;
	unsigned char* dstRow = dst;

	// draw pixels at once
	if (rectX == 0 && rectY == 0 &&
		rectWidth == srcWidth && rectHeight == srcHeight &&
		rectWidth == dstWidth && rectHeight == dstHeight &&
		srcBytesPerRow == dstBytesPerRow) {
		memcpy(dst, src, dstHeight*dstBytesPerRow);
		return;
	}

	// draw pixels row by row
	dstRow += rectY*dstBytesPerRow + rectX*bytesPerPixel;
	if (rectWidth == srcWidth && rectHeight == srcHeight) {
		for (size_t h = 0; h < rectHeight; ++h) {
			memcpy(dstRow, srcRow, rectWidth*bytesPerPixel);
			srcRow += srcBytesPerRow;
			dstRow += dstBytesPerRow;
		}
		return;
	} 

	// draw pixel by pixel
	double scaleWidth = (double)srcWidth/rectWidth;
	double scaleHeight = (double)srcHeight/rectHeight;
	for (size_t h = 0; h < rectHeight; ++h) {
		size_t srcH = floor(h*scaleHeight);
		const unsigned char* srcPixel = srcRow + srcH*srcBytesPerRow;
		unsigned char* dstPixel = dstRow;
		for (size_t w = 0; w < rectWidth; ++w) {
			size_t srcW = floor(w*scaleWidth);
			memcpy(dstPixel, srcPixel + srcW*bytesPerPixel, bytesPerPixel);
			dstPixel += bytesPerPixel;
		}
		dstRow += dstBytesPerRow;
	}
}

@implementation Snooper

@synthesize cursorRed;
@synthesize cursorGreen;
@synthesize cursorBlue;
@synthesize cursorAlpha;
@synthesize highlight;
@synthesize statisticsInfo;
@synthesize graph;

- (id) init 
{
	if ((self = [super init])) {
		capturePointWidth = 0;
		capturePointHeight = 0;
		zoomPointWidth = 0;
		zoomPointHeight = 0;
		viewPointWidth = 0;
		viewPointHeight = 0;

		capturePixelCenterX = 0;
		capturePixelCenterY = 0;

		zoomPixelCenterX = 0;
		zoomPixelCenterY = 0;

		gridPixelSize = 0;

		capturePixelWidth = 0;
		capturePixelHeight = 0;
		zoomPixelWidth = 0;
		zoomPixelHeight = 0;
		viewPixelWidth = 0;
		viewPixelHeight = 0;
		cropPixelX = 0;
		cropPixelY = 0;

		captureContext = nil;
		zoomContext = nil;
		viewContext = nil;

		cursorPointX = 0;
		cursorPointY = 0;

		maxBackingScaleFactor = 0.0;
		capturePointRect = NSZeroRect;

		colorSpace = CGColorSpaceCreateDeviceRGB();
		bitsPerComponent = 8;
		bitsPerPixel = bitsPerComponent*4;
		bytesPerPixel = bitsPerPixel/8;
		alphaInfo = kCGImageAlphaPremultipliedFirst;
		bitmapInfo = kCGBitmapByteOrder32Little | alphaInfo;

		captureData = NULL;
		captureDataWidth = 0;
		captureDataHeight = 0;
		captureDataBytesPerRow = 0;

		cgRed = CGColorCreateGenericRGB(1.0, 0.0, 0.0, 1.0);
		cgGreen = CGColorCreateGenericRGB(0.0, 1.0, 0.0, 1.0);
		cgBlue = CGColorCreateGenericRGB(0.0, 0.0, 1.0, 1.0);
		cgWhite = CGColorGetConstantColor(kCGColorWhite);
		cgBlack = CGColorGetConstantColor(kCGColorBlack);
		cgGray = CGColorCreateGenericRGB(0.15, 0.15, 0.15, 1.0);

		statisticsInfoString[0] = '\0';
	}
	return self;
}

- (void) dealloc
{
	if (colorSpace) {
		CGColorSpaceRelease(colorSpace);
		colorSpace = NULL;
	}
	if (captureData) {
		free(captureData);
		captureData = NULL;
	}

	CGColorRelease(cgRed);
	CGColorRelease(cgGreen);
	CGColorRelease(cgBlue);
	CGColorRelease(cgGray);
	[super dealloc];
}

- (void) setCursorPointX:(int)x Y:(int)y
{
	if (cursorPointX != x || cursorPointY != y) {
		cursorPointX = x;
		cursorPointY = y;
		[self resetCapturePointRect];
	}
}

- (void) setViewPointWidth:(size_t)width Height:(size_t)height
{
	if (viewPointWidth != width || viewPointHeight != height) {
		viewPointWidth = width;
		viewPointHeight = height;
		[self resetPointSizes];
		[self resetCapturePointRect];
	}
}

- (void) setZoom:(size_t)z
{
	if (zoom != z) {
		zoom = z;
		[self resetPointSizes];
		[self resetCapturePointRect];
	}
}

- (void) resetPointSizes
{
	capturePointWidth = ceil((double)viewPointWidth/zoom);
	capturePointHeight = ceil((double)viewPointHeight/zoom);
	zoomPointWidth = capturePointWidth*zoom;
	zoomPointHeight = capturePointHeight*zoom;
	[self resetPixelSizes];
#ifndef NDEBUG
	NSLog(@"capturePoint: {%zu, %zu}", capturePointWidth, capturePointHeight);
	NSLog(@"zoomPoint: {%zu, %zu}", zoomPointWidth, zoomPointHeight);
	NSLog(@"viewPoint: {%zu, %zu}", viewPointWidth, viewPointHeight);
#endif
}

- (void) resetPixelSizes
{
	viewPixelWidth = ceil(viewPointWidth*maxBackingScaleFactor);
	viewPixelHeight = ceil(viewPointHeight*maxBackingScaleFactor);
	capturePixelWidth = ceil(capturePointWidth*maxBackingScaleFactor);
	capturePixelHeight = ceil(capturePointHeight*maxBackingScaleFactor);
	zoomPixelWidth = ceil(zoomPointWidth*maxBackingScaleFactor);
	zoomPixelHeight = ceil(zoomPointHeight*maxBackingScaleFactor);
	cropPixelX  = zoomPixelWidth - viewPixelWidth;
	cropPixelY = zoomPixelHeight - viewPixelHeight;

	gridPixelSize = zoom*maxBackingScaleFactor;

	// origin is upper-left
	capturePixelCenterX = floor((capturePointWidth/2)*maxBackingScaleFactor);
	capturePixelCenterY = floor((capturePointHeight/2)*maxBackingScaleFactor);

	// origin is bottom-left
	zoomPixelCenterX = capturePixelCenterX*zoom;
	zoomPixelCenterY = zoomPixelHeight - capturePixelCenterY*zoom - gridPixelSize;

#ifndef NDEBUG
	NSLog(@"capturePixel: {%zu, %zu}", capturePixelWidth, capturePixelHeight);
	NSLog(@"zoomPixel: {%zu, %zu}", zoomPixelWidth, zoomPixelHeight);
	NSLog(@"cropPixel: {%zu, %zu}", cropPixelX, cropPixelY);
	NSLog(@"viewPixel: {%zu, %zu}", viewPixelWidth, viewPixelHeight);

	NSLog(@"gridPixelSize: %zu", gridPixelSize);
	NSLog(@"capturePixelCenter: {%zu, %zu}", capturePixelCenterX, capturePixelCenterY);
	NSLog(@"zoomPixelCenter: {%zu, %zu}", zoomPixelCenterX, zoomPixelCenterY);
#endif
}

- (void) resetCapturePointRect
{
	capturePointRect.origin.x = cursorPointX - floor(capturePointWidth/2.0);
	capturePointRect.origin.y = cursorPointY - ceil(capturePointHeight/2.0);
	capturePointRect.size.width = capturePointWidth;
	capturePointRect.size.height = capturePointHeight;
}

- (unsigned char*) initCaptureData
{
	if (capturePixelWidth == 0 || capturePixelHeight == 0)
		return NULL;

	size_t bytes = capturePixelWidth*capturePixelHeight*bytesPerPixel;
	if (captureDataWidth != capturePixelWidth ||
		captureDataHeight != capturePixelHeight ||
		captureData == NULL) {
		if (captureData)
			free(captureData);
		captureData = malloc(bytes);
		if (captureData == NULL) {
			captureDataWidth = 0;
			captureDataHeight = 0;
			captureDataBytesPerRow = 0;
			return NULL;
		}
		captureDataWidth = capturePixelWidth;
		captureDataHeight = capturePixelHeight;
		captureDataBytesPerRow = capturePixelWidth*bytesPerPixel;
	}
	bzero(captureData, bytes);
	clearBitmapData(captureData, captureDataWidth, captureDataHeight, bytesPerPixel, captureDataBytesPerRow);
	return captureData;
}

- (CGContextRef) initCaptureContextWithImage:(CGImageRef)screenshot
{
	if (capturePixelWidth == 0 || capturePixelHeight == 0)
		return nil;
	if (captureContext == nil ||
		CGBitmapContextGetWidth(captureContext) != capturePixelWidth ||
		CGBitmapContextGetHeight(captureContext) != capturePixelHeight
	) {
#ifndef NDEBUG
		NSLog(@"context = CGBitmapContextCreate(%zu, %zu)", capturePixelWidth, capturePixelHeight);
#endif
		if (captureContext != nil)
			CGContextRelease(captureContext);

		// use capture image's color space
		if (!colorSpace)
			colorSpace = CGImageGetColorSpace(screenshot);

#ifndef DEBUG
		NSLog(@"Color space: %@", colorSpace);
		NSLog(@"AlphaInfo: %d", CGImageGetAlphaInfo(screenshot));
		NSLog(@"BitmapInfo: %d", CGImageGetBitmapInfo(screenshot));
		NSLog(@"BitsPerComponent: %zu", CGImageGetBitsPerComponent(screenshot));
		NSLog(@"BitsPerPixel: %zu",  CGImageGetBitsPerPixel(screenshot));
#endif
		captureContext = CGBitmapContextCreate(
			NULL,
			capturePixelWidth, capturePixelHeight,
			CGImageGetBitsPerComponent(screenshot),
			// bitsPerComponent,
			0,
			colorSpace,
			bitmapInfo
		);
	}
	// set background to black
	CGContextSetBlendMode(zoomContext, kCGBlendModeNormal);
	CGContextSetFillColorWithColor(captureContext, cgBlack);
	CGRect rect = {CGPointZero, {capturePixelWidth, capturePixelHeight}};
	CGContextFillRect(captureContext, rect);
	return captureContext;
}

- (CGContextRef) initZoomContext
{
	if (zoomPixelWidth == 0 || zoomPixelHeight == 0)
		return nil;
	if (zoomContext == nil ||
		CGBitmapContextGetWidth(zoomContext) != zoomPixelWidth ||
		CGBitmapContextGetHeight(zoomContext) != zoomPixelHeight
	) {
#ifndef NDEBUG
		NSLog(@"zoomContext = CGBitmapContextCreate(%zu, %zu)", zoomPixelWidth, zoomPixelHeight);
#endif
		if (zoomContext != nil)
			CGContextRelease(zoomContext);

		zoomContext = CGBitmapContextCreate(
			NULL,
			zoomPixelWidth, zoomPixelHeight,
			bitsPerComponent,
			0,
			colorSpace,
			bitmapInfo
		);
		CGContextSetInterpolationQuality(zoomContext, kCGInterpolationNone);
	}
	return zoomContext;
}

- (void) captureCursorColor
{
#ifdef CAPTURE_USING_CG_DISPLAY_CREATE_IMAGE_FOR_RECT_BITMAP_DATA
	unsigned char *data = captureData +
		capturePixelCenterY*captureDataBytesPerRow +
		capturePixelCenterX*bytesPerPixel;
	cursorBlue = data[BLUE_INDEX];
	cursorGreen = data[GREEN_INDEX];
	cursorRed = data[RED_INDEX];
	cursorAlpha = data[ALPHA_INDEX];
#endif

#ifdef CAPTURE_USING_CG_DISPLAY_CREATE_IMAGE_FOR_RECT_BITMAP_CONTEXT
	size_t bitsPerPixel = CGBitmapContextGetBitsPerPixel(captureContext);
	size_t bytesPerPixel = bitsPerPixel/8;
	const unsigned char *data = CGBitmapContextGetData(captureContext);
	size_t bytesPerRow = CGBitmapContextGetBytesPerRow(captureContext);
	data += (capturePixelHeight/2)*bytesPerRow + (capturePixelWidth/2)*bytesPerPixel;
	cursorBlue = data[BLUE_INDEX];
	cursorGreen = data[GREEN_INDEX];
	cursorRed = data[RED_INDEX];
	cursorAlpha = data[ALPHA_INDEX];
#endif

#ifndef NDEBUG
	NSLog(@"color: %zu, %zu, %zu, %zu", cursorRed, cursorGreen, cursorBlue, cursorAlpha);
#endif
}

- (CGContextRef) getZoomContext
{
	if ([self initZoomContext] == nil) {
		NSLog(@"[ERROR] zoomContext is nil");
		return nil;
	}
	CGImageRef captureImage = CGBitmapContextCreateImage(captureContext);
	if (captureImage == nil) {
		NSLog(@"[ERROR] captureImage is nil");
		return nil;
	}

	// zoom it
	CGRect rect = {CGPointZero, {zoomPixelWidth, zoomPixelHeight}};
	CGContextSetBlendMode(zoomContext, kCGBlendModeNormal);
	CGContextDrawImage(zoomContext, rect, captureImage);
#ifndef NDEBUG
	NSLog(@"CGContextDrawImage(zoomContext, %@, captureImage", NSStringFromRect(NSRectFromCGRect(rect)));
#endif

	CGImageRelease(captureImage);
	return zoomContext;
}

- (CGImageRef) getCropImage
{
	CGImageRef zoomImage = CGBitmapContextCreateImage(zoomContext);
	if (zoomImage == nil) {
		NSLog(@"[ERROR] zoomImage is nil");
		return nil;
	}
	if (cropPixelX > 0 || cropPixelY > 0) {
		// CGRect rect = {{0, cropPixelY}, {viewPixelWidth, viewPixelHeight}};

		// origin is upper-left!!!!!!
		CGRect rect = {{0, 0}, {viewPixelWidth, viewPixelHeight}};
		CGImageRef cropImage = CGImageCreateWithImageInRect(zoomImage, rect);
		CGImageRelease(zoomImage);
#ifndef NDEBUG
		NSLog(@"image = CGImageCreateWithImageInRect(scaledImage, %@)", NSStringFromRect(NSRectFromCGRect(rect)));
#endif
		return cropImage;
	} 
	return zoomImage;
}

- (CGImageRef) getScreenshotFor:(id)screen In:(CGRect)rect
{
#ifndef NDEBUG
	NSLog(@"capturePixel: {%zu, %zu}", capturePixelWidth, capturePixelHeight);
	NSLog(@"zoomPixel: {%zu, %zu}", zoomPixelWidth, zoomPixelHeight);
	NSLog(@"cropPixel: {%zu, %zu}", cropPixelX, cropPixelY);
	NSLog(@"viewPixel: {%zu, %zu}", viewPixelWidth, viewPixelHeight);

	NSLog(@"gridPixelSize: %zu", gridPixelSize);
	NSLog(@"capturePixelCenter: {%zu, %zu}", capturePixelCenterX, capturePixelCenterY);
	NSLog(@"zoomPixelCenter: {%zu, %zu}", zoomPixelCenterX, zoomPixelCenterY);
	NSLog(@"CGDisplayCreateImageForRect: %@", NSStringFromRect(NSRectFromCGRect(rect)));
#endif
	NSDictionary *description = [screen deviceDescription];
	CGDirectDisplayID displayID = (CGDirectDisplayID)[[description valueForKey:@"NSScreenNumber"] unsignedIntValue];
	return CGDisplayCreateImageForRect(displayID, rect);
}

- (CGContextRef) getCaptureContextFrom:(NSArray*)screens
{
	/**
	 * Use CGDisplayCreateImage for 10.6+
	 * https://developer.apple.com/library/mac/#qa/qa1741/_index.html
	 */
	
#ifdef CAPTURE_USING_CG_WINDOW_LIST_CREATE_IMAGE
	CGRect rect = {
		{capturePointRect.origin.x, mainScreenHeight - capturePointRect.origin.y - capturePointRect.size.height},
		{capturePointRect.size.width, capturePointRect.size.height}
	};
	NSLog(@"CGWindowListCreateImage(%@, ...)", NSStringFromRect(NSRectFromCGRect(rect)));
	CGImageRef screenshot = CGWindowListCreateImage(
		rect, kCGWindowListOptionAll,
		kCGNullWindowID, kCGWindowImageBestResolution
	);
	CGRect location = {
		{0, 0},
		{capturePointRect.size.width*maxBackingScaleFactor, capturePointRect.size.height*maxBackingScaleFactor}
	};	
	[self drawImage:screenshot At:location];
	if (captureContext)
		CGContextRelease(captureContext);
	captureContext = CGBitmapContextCreate(
		captureData, captureDataWidth, captureDataHeight,
		bitsPerComponent, captureDataBytesPerRow,
		colorSpace, bitmapInfo
	);
	return captureContext;
#endif

#ifdef CAPTURE_USING_CG_DISPLAY_CREATE_IMAGE_FOR_RECT_BITMAP_DATA
	if ([self initCaptureData] == NULL) {
		NSLog(@"[ERROR]: captureData is NULL");
		return nil;
	}
#endif

#ifdef CAPTURE_USING_CG_DISPLAY_CREATE_IMAGE_FOR_RECT_BITMAP_CONTEXT
	BOOL first = YES;
#endif
	for (id screen in screens) {
		NSRect screenRect = [screen frame];
#ifndef NDEBUG
		NSLog(@"screenRect (point): %@", NSStringFromRect(screenRect));
		NSLog(@"capturePointRect: %@", NSStringFromRect(capturePointRect));
#endif
		NSRect is = NSIntersectionRect(screenRect, capturePointRect);
		// if capturePointRect overlaps this screen, capture it
		if (is.size.width > 0 && is.size.height > 0) {
#ifndef NDEBUG
			NSLog(@"intersection: %@", NSStringFromRect(is));
#endif
			// display space: origin is at upper-left
			CGRect rect = {
				{
					is.origin.x - screenRect.origin.x, 
					(screenRect.size.height - is.size.height) - 
						(is.origin.y - screenRect.origin.y)
				},
				{is.size.width, is.size.height}
			};
			CGImageRef screenshot = [self getScreenshotFor:screen In:rect];
			if (screenshot == nil) {
				NSLog(@"[ERROR] screenshot is nil.");
				continue;
			}

#ifdef CAPTURE_USING_CG_DISPLAY_CREATE_IMAGE_FOR_RECT_BITMAP_CONTEXT
			if (first) {
				if ([self initCapturedContextWithImage:screenshot] == nil) {
					NSLog(@"[ERROR]: capturedContext is nil");
					continue;
				}
				first = NO;
			}
#endif

			// place screenshot at the correct location
			size_t width = is.size.width*maxBackingScaleFactor;
			size_t height = is.size.height*maxBackingScaleFactor;
			size_t x = (is.origin.x-capturePointRect.origin.x)*maxBackingScaleFactor;

#ifdef CAPTURE_USING_CG_DISPLAY_CREATE_IMAGE_FOR_RECT_BITMAP_DATA
			// origin is at upper-left corner 
			size_t y = capturePixelHeight - (is.origin.y-capturePointRect.origin.y)*maxBackingScaleFactor - height;
#endif
			
#ifdef CAPTURE_USING_CG_DISPLAY_CREATE_IMAGE_FOR_RECT_BITMAP_CONTEXT
			// origin is at bottom-left corner 
			size_t y = (is.origin.y-capturePointRect.origin.y)*maxBackingScaleFactor;
#endif
			
			CGRect location = {{x, y}, {width, height}};

#ifdef CAPTURE_USING_CG_DISPLAY_CREATE_IMAGE_FOR_RECT_BITMAP_DATA
#ifndef NDEBUG
			NSLog(@"[self drawImage:screenshot At:%@]", NSStringFromRect(NSRectFromCGRect(location)));
#endif
			[self drawImage:screenshot At:location]; // origin is at upper-left
#endif

#ifdef CAPTURE_USING_CG_DISPLAY_CREATE_IMAGE_FOR_RECT_BITMAP_CONTEXT
#ifndef NDEBUG
			NSLog(@"CGContextDrawImage(context, %@, screenshot)", NSStringFromRect(NSRectFromCGRect(location)));
#endif
			CGContextDrawImage(captureContext, location, screenshot); // origin is at bottom-left
#endif
			CGImageRelease(screenshot);
		}
	}
#ifdef CAPTURE_USING_CG_DISPLAY_CREATE_IMAGE_FOR_RECT_BITMAP_DATA
	if (captureContext)
		CGContextRelease(captureContext);
	captureContext = CGBitmapContextCreate(
		captureData, captureDataWidth, captureDataHeight,
		bitsPerComponent, captureDataBytesPerRow,
		colorSpace, bitmapInfo
	);
#endif

#ifdef CAPTURE_USING_CG_DISPLAY_CREATE_IMAGE_FOR_RECT_BITMAP_CONTEXT
	if (first == YES)
		return nil;
#endif
	return captureContext;
}

- (NSImage*) snoopFrom:(NSArray*)screens
{
	// capture 
	if ([self getCaptureContextFrom:screens] == nil) {
		NSLog(@"[ERROR] getCaptureContextFrom:screens failed.");
		return nil;
	}

	// zoom
	if ([self getZoomContext] == nil) {
		NSLog(@"[ERROR] getZoomContext failed.");
		return nil;
	}

	// post processing
	[self captureCursorColor];
	[self drawGraph];
	[self drawCenter];

	// crop
	CGImageRef cropImage = [self getCropImage];
	if (cropImage == nil)
		return nil;
	
	NSImage *viewImage = [[NSImage alloc] initWithCGImage:cropImage size:NSMakeSize(viewPointWidth, viewPointHeight)];
	CGImageRelease(cropImage);
	return viewImage;
}

- (void) drawGraph 
{
	CGRect rect = {{zoomPixelCenterX, zoomPixelCenterY}, {gridPixelSize, gridPixelSize}};
	BOOL showHighlight = NO;
	switch (graph) {
		case GSNone:
			break;
		case GSHoriStep:
		case GSHoriLinear:
			[self drawHorizontalGraph];
			rect.origin.x = 0;
			rect.size.width = zoomPixelWidth;
			showHighlight = YES;
			break;
		case GSVertStep:
		case GSVertLinear:
			[self drawVerticalGraph];
			rect.origin.y = 0;
			rect.size.height = zoomPixelHeight;
			showHighlight = YES;
			break;
		case GSStatistics:
			[self drawStatistics];
			break;
	}
	if (showHighlight && highlight) {
#ifndef NDEBUG
		NSLog(@"highlight: %@", NSStringFromRect(NSRectFromCGRect(rect)));
#endif
		CGContextSetBlendMode(zoomContext, kCGBlendModePlusLighter);
		CGContextSetFillColorWithColor(zoomContext, cgGray);
		CGContextFillRect(zoomContext, rect);
	}
}

- (void) drawStatistics
{
	int size = capturePixelWidth*capturePixelHeight;
	int size2 = size/2;

	size_t histogram[3][256] = {{0}};
	size_t peakCount[3] = {0}, peakValue[3] = {0}, maxCount = 0;
	size_t mean[3] = {0}, mediam[3] = {0};
	size_t min[3] = {255, 255, 255}, max[3] = {0};

	// statistics {{{
	const unsigned char* row = captureData;
	for (size_t y = 0; y < capturePixelHeight; ++y) {
		const unsigned char* pixel = row;
		for (size_t x = 0; x < capturePixelWidth; ++x) {
			for (size_t c = 0; c < 3; ++c) {
				unsigned char v = pixel[c];
				++histogram[c][v];
				if (histogram[c][v] > peakCount[c]) {
					peakValue[c] = v;
					peakCount[c] = histogram[c][v];
				}
				mean[c] += v;
				if (v > max[c])
					max[c] = v;
				else if (v < min[c])
					min[c] = v;
			}
			pixel += bytesPerPixel;
		}
		row += captureDataBytesPerRow;
	}

	for (size_t c = 0; c < 3; ++c) {
		if (peakCount[c] > maxCount)
			maxCount = peakCount[c];
		mean[c] /= size;
		size_t count = 0;
		for (size_t v = 0; v < 256; ++v) {
			count += histogram[c][v];
			if (count >= size2) {
				mediam[c] = v;
				break;
			}
		}
	}
	// }}}

	size_t spacing = SPACING*maxBackingScaleFactor;
	CGRect rect = {
		{spacing, zoomPixelHeight - spacing},
		{255*maxBackingScaleFactor, viewPixelHeight/3}
	};
	if (rect.size.height > 255*maxBackingScaleFactor)
		rect.size.height = 255*maxBackingScaleFactor;
	rect.origin.y -= rect.size.height;

	[self drawGraphCanvasWithRect:rect];

	CGContextSetBlendMode(zoomContext, kCGBlendModeLighten);
	CGContextSetLineWidth(zoomContext, maxBackingScaleFactor);
	CGColorRef colors[3] = {cgBlue, cgGreen, cgRed};
	for (size_t c = 0; c < 3; ++c) {
		CGContextSetStrokeColorWithColor(zoomContext, colors[c]);
		CGContextMoveToPoint(zoomContext, rect.origin.x, rect.origin.y + (size_t)(rect.size.height*histogram[c][0]/maxCount));
		for (size_t x = maxBackingScaleFactor, v = 1; x <= rect.size.width; x += maxBackingScaleFactor, ++v) {
			size_t y = rect.origin.y + (size_t)(rect.size.height*histogram[c][v]/maxCount);
			CGContextAddLineToPoint(zoomContext, rect.origin.x + x, y);
		}
		CGContextStrokePath(zoomContext);
	}

	if (statisticsInfo) {
		sprintf(statisticsInfoString, 
			"peak=%03zu(%zu),%03zu(%zu),%03zu(%zu)\n"
			"min=(%03zu,%03zu,%03zu),max=(%03zu,%03zu,%03zu)\n"
			"avg=(%03zu,%03zu,%03zu),med=(%03zu,%03zu,%03zu)",
			peakValue[RED_INDEX], peakCount[RED_INDEX],
			peakValue[GREEN_INDEX], peakCount[GREEN_INDEX],
			peakValue[BLUE_INDEX], peakCount[BLUE_INDEX],

			min[RED_INDEX], min[GREEN_INDEX], min[BLUE_INDEX], 
			max[RED_INDEX], max[GREEN_INDEX], max[BLUE_INDEX],

			mean[RED_INDEX], mean[GREEN_INDEX], mean[BLUE_INDEX], 
			mediam[RED_INDEX], mediam[GREEN_INDEX], mediam[BLUE_INDEX] 
		);
	}
}

- (void) drawCenter
{
	if (zoom*maxBackingScaleFactor <= 2)
		return;

	CGRect rect = {{zoomPixelCenterX, zoomPixelCenterY}, {gridPixelSize, gridPixelSize}};
#ifndef NDEBUG
	NSLog(@"drawCenter: %@", NSStringFromRect(rect));
#endif
	CGContextSetBlendMode(zoomContext, kCGBlendModeNormal);
	CGContextSetStrokeColorWithColor(zoomContext, cgRed);
	CGContextStrokeRectWithWidth(zoomContext, rect, maxBackingScaleFactor);
}

- (void) drawHorizontalGraph
{
	size_t spacing = SPACING*maxBackingScaleFactor;
	CGRect rect = {
		{spacing, zoomPixelHeight - spacing},
		{viewPixelWidth - 2*spacing, viewPixelHeight/3}
	};
	if (rect.size.height > 255*maxBackingScaleFactor)
		rect.size.height = 255*maxBackingScaleFactor;
	rect.origin.y -= rect.size.height;

	[self drawGraphCanvasWithRect:rect];

	// highlight
	if (highlight) {
		CGRect hlRect = {
			{zoomPixelCenterX, rect.origin.y},
			{gridPixelSize, rect.size.height}
		};
		CGContextSetFillColorWithColor(zoomContext, cgGray);
		CGContextFillRect(zoomContext, hlRect);
	}

	CGContextSetBlendMode(zoomContext, kCGBlendModeLighten);
	CGColorRef colors[3] = {cgBlue, cgGreen, cgRed};
	const unsigned char* row = captureData + capturePixelCenterY*captureDataBytesPerRow;
	if (graph == GSHoriStep) {
		for (size_t x = rect.origin.x; x <= rect.origin.x + rect.size.width; ++x) {
			const unsigned char* pixel = row + (x/zoom)*bytesPerPixel;
			for (size_t c = 0; c < 3; ++c) {
				size_t y = rect.size.height*pixel[c]/255;
				CGRect stepRect = {
					{x, rect.origin.y + y},
					{1, maxBackingScaleFactor}
				};
				CGContextSetFillColorWithColor(zoomContext, colors[c]);
				CGContextFillRect(zoomContext, stepRect);
			}
		}
	} else {
		size_t realZoom = zoom/maxBackingScaleFactor;
		if (realZoom == 0)
			realZoom = 1;

		CGContextSetLineWidth(zoomContext, maxBackingScaleFactor);
		for (size_t c = 0; c < 3; ++c) {
			const unsigned char* pixel = row + (size_t)(rect.origin.x/zoom)*bytesPerPixel;
			CGContextSetStrokeColorWithColor(zoomContext, colors[c]);
			CGContextMoveToPoint(zoomContext, rect.origin.x, rect.origin.y + (size_t)(rect.size.height*pixel[c]/255));

			size_t x = rect.origin.x + (size_t)1.5*(realZoom) - (size_t)rect.origin.x % realZoom;
			for (; x <= rect.origin.x + rect.size.width; x += realZoom) {
				pixel = row + (x/zoom)*bytesPerPixel;
				size_t y = rect.origin.y + rect.size.height*pixel[c]/255;
				CGContextAddLineToPoint(zoomContext, x, y);
			}
			x -= realZoom;
			if (x < rect.origin.x + rect.size.width) {
				pixel = row + ((size_t)(rect.origin.x + rect.size.width)/zoom)*bytesPerPixel;
				size_t y = rect.origin.y + rect.size.height*pixel[c]/255;
				CGContextAddLineToPoint(zoomContext, x, y);
			}
			CGContextStrokePath(zoomContext);
		}
	}
}

- (void) drawVerticalGraph
{
	size_t spacing = SPACING*maxBackingScaleFactor;
	CGRect rect = {
		{viewPixelWidth - spacing, cropPixelY + spacing},
		{viewPixelWidth/3, viewPixelHeight - 2*spacing}
	};
	if (rect.size.width > 255*maxBackingScaleFactor)
		rect.size.width = 255*maxBackingScaleFactor;
	rect.origin.x -= rect.size.width;
	[self drawGraphCanvasWithRect:rect];

	// highlight
	if (highlight) {
		CGRect hlRect = {{rect.origin.x, zoomPixelCenterY}, {rect.size.width, gridPixelSize}};
		CGContextSetFillColorWithColor(zoomContext, cgGray);
		CGContextFillRect(zoomContext, hlRect);
	}

	CGContextSetBlendMode(zoomContext, kCGBlendModeLighten);
	CGColorRef colors[3] = {cgBlue, cgGreen, cgRed};
	const unsigned char* row = captureData + capturePixelCenterX*bytesPerPixel;
	if (graph == GSVertStep) {
		for (size_t y = rect.origin.y; y <= rect.origin.y + rect.size.height; ++y) {
			size_t invy = zoomPixelHeight - y;
			const unsigned char* pixel = row + (invy/zoom)*captureDataBytesPerRow;
			for (size_t c = 0; c < 3; ++c) {
				size_t x = rect.size.width*pixel[c]/255;
				CGRect stepRect = {
					{rect.origin.x + x, y},
					{maxBackingScaleFactor, 1}
				};
				CGContextSetFillColorWithColor(zoomContext, colors[c]);
				CGContextFillRect(zoomContext, stepRect);
			}
		}
	} else {
		size_t realZoom = zoom/maxBackingScaleFactor;
		if (realZoom == 0)
			realZoom = 1;

		CGContextSetLineWidth(zoomContext, maxBackingScaleFactor);
		for (size_t c = 0; c < 3; ++c) {
			const unsigned char* pixel = row + (size_t)((zoomPixelHeight - rect.origin.y)/zoom)*captureDataBytesPerRow;
			CGContextSetStrokeColorWithColor(zoomContext, colors[c]);
			CGContextMoveToPoint(zoomContext, rect.origin.x + (size_t)(rect.size.width*pixel[c]/255), rect.origin.y);

			size_t y = rect.origin.y + (size_t)1.5*(realZoom) - (size_t)rect.origin.y % realZoom;
			for (; y <= rect.origin.y + rect.size.height; y += realZoom) {
				pixel = row + ((size_t)(zoomPixelHeight - y)/zoom)*captureDataBytesPerRow;
				size_t x = rect.origin.x + rect.size.width*pixel[c]/255;
				CGContextAddLineToPoint(zoomContext, x, y);
			}
			y -= realZoom;
			if (y < rect.origin.y + rect.size.height) {
				pixel = row + ((size_t)(rect.origin.y + rect.size.height)/zoom)*captureDataBytesPerRow;
				size_t x = rect.origin.x + rect.size.width*pixel[c]/255;
				CGContextAddLineToPoint(zoomContext, x, y);
			}
			CGContextStrokePath(zoomContext);
		}
	}
}

- (void) drawGraphCanvasWithRect:(CGRect)rect
{
	// black blackground
	CGContextSetBlendMode(zoomContext, kCGBlendModeNormal);
	CGContextSetFillColorWithColor(zoomContext, cgBlack);
	CGContextFillRect(zoomContext, rect);

	// white border
	CGRect borderRect = {
		{rect.origin.x - maxBackingScaleFactor, rect.origin.y - maxBackingScaleFactor},
		{rect.size.width + 2*maxBackingScaleFactor, rect.size.height + 2*maxBackingScaleFactor}
	};
	CGContextSetStrokeColorWithColor(zoomContext, cgWhite);
	CGContextStrokeRectWithWidth(zoomContext, borderRect, maxBackingScaleFactor);
}

/*
- (unsigned char*) getNormalizedBitmapDataFrom:(CGImageRef)image
{
	CGImageAlphaInfo ai = CGImageGetAlphaInfo(image);
	CGBitmapInfo bi = CGImageGetBitmapInfo(image);
	size_t bpc = CGImageGetBitsPerComponent(image);
	size_t bpp = CGImageGetBitsPerPixel(image);
	size_t bpr = CGImageGetBytesPerRow(image);
	size_t width = CGImageGetWidth(image);
	size_t height = CGImageGetHeight(image);

	if (bitmapInfo == bi) {
		CFDataRef cfData = CGDataProviderCopyData(CGImageGetDataProvider(image));
		NSData* nsData = (__bridge NSData*)cfData;
		*data = (const unsigned char*)CFDataGetBytePtr(cfData);
		return 
	}

	switch (alphaInfo) {
	case kCGImageAlphaNone:
	case kCGImageAlphaOnly :
		// unsupported format
		return NULL;
	case kCGImageAlphaPremultipliedLast:
	case kCGImageAlphaLast:
	case kCGImageAlphaNoneSkipLast:
		break;
	case kCGImageAlphaPremultipliedFirst:
	case kCGImageAlphaFirst:
	case kCGImageAlphaNoneSkipFirst:
		break;
	}
}
*/

- (void) drawImage:(CGImageRef)image At:(CGRect)rect
{
	CGBitmapInfo bi = CGImageGetBitmapInfo(image);
	if (bitmapInfo == bi) {
		CFDataRef cfData = CGDataProviderCopyData(CGImageGetDataProvider(image));
		const unsigned char *data = (const unsigned char*)CFDataGetBytePtr(cfData);
		drawBitmapData(
			data,
			CGImageGetWidth(image), CGImageGetHeight(image),
			CGImageGetBytesPerRow(image),
			captureData,
			capturePixelWidth, capturePixelHeight,
			captureDataBytesPerRow, bytesPerPixel,
			rect.origin.x, rect.origin.y, rect.size.width, rect.size.height
		);
		CFRelease(cfData);
	} else { 
		// TODO: to be implemented!
	}
}

- (void) setMainScreenHeight:(size_t)height
{
	mainScreenHeight = height;
}

- (void) setMaxBackingScaleFactor:(CGFloat)factor
{
	if (factor != maxBackingScaleFactor) {
#ifndef NDEBUG
		NSLog(@"maxBackingScaleFactor: %f", maxBackingScaleFactor);
#endif
		maxBackingScaleFactor = factor;
		[self resetPixelSizes];
	}
}

- (NSString*) createStatisticsInfoString
{
	return [[NSString alloc] initWithCString:statisticsInfoString encoding:NSASCIIStringEncoding];
}

@end
